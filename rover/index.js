module.exports = function Rover (boundary, locationDirection, moves) {
  this.boundary = boundary
  this.locationDirection = locationDirection
  this.moves = moves

  this.move = function () {
    this.moves = this.moves.toUpperCase()
    for (const step of this.moves) {
      if (step === 'M') {
        this.moveInDirection()
      } else if (step === 'L' || step === 'R') {
        this.changeDirection(step)
      } else {
        console.log(this.getLocation())
        throw new Error('Invalid move')
      }
    }
  }

  this.moveInDirection = function () {
    const direction = this.locationDirection.direction
    const testMovement = this.locationDirection

    switch (direction) {
      case 'E':
        ++testMovement.x
        break
      case 'W':
        --testMovement.x
        break
      case 'N':
        ++testMovement.y
        break
      case 'S':
        --testMovement.y
        break
    }

    if (testMovement.x < 0 || testMovement.x > this.boundary.x) {
      console.log(this.getLocation())
      throw new Error('Invalid x move')
    }
    if (testMovement.y < 0 || testMovement.y > this.boundary.y) {
      console.log(this.getLocation())
      throw new Error('Invalid y move')
    }
    this.locationDirection = testMovement
  }

  this.changeDirection = function (changeToDirection) {
    switch (this.locationDirection.direction) {
      case 'N':
        changeToDirection = changeToDirection === 'R' ? 'E' : 'W'
        break
      case 'E':
        changeToDirection = changeToDirection === 'R' ? 'S' : 'N'
        break
      case 'S':
        changeToDirection = changeToDirection === 'R' ? 'W' : 'E'
        break
      case 'W':
        changeToDirection = changeToDirection === 'R' ? 'E' : 'S'
        break
    }
    this.locationDirection.direction = changeToDirection
  }

  this.getLocation = function () {
    const destination = this.locationDirection
    return destination.x + ' ' + destination.y + ' ' + destination.direction
  }
}
