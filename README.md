# Mars Rover Challenge

Functions to control rover movements

## Getting Started

These instructions will get the app running.
When the app is running, it will prompt for input from the user.

The application firstly captures and validates input before it is sent to the rover for execution.
All input to the rover should be sent through the console prompts

### Prerequisites

What you need to run the application

[Node.js](https://nodejs.org/en/)

### Installing

Run the following commands

```
npm install
```

Then run

```
npm start
```

Start typing input with valid formarting. Invalid formats will be rejected.


## Running tests

```
npm test
```

### Code compliance

Install [Standard](https://standardjs.com/) and run this connand

```
standard --fix
```

### Justifications

1. Sequential user input is required to validate input before rover is given instructions to execute.
2. Rover tests if movement is within bounds before executing movement.
3. JS closures used incase more than one rover is available
4. Rover rejects invalid move and shows current location with error