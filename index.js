
const readline = require('readline')
const Rover = require('./rover')

const lineReader = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const getBoundary = 'Please type boundary and press enter  e.g 8 8 > '
const getLocationDirection = 'Please type current location and direction e.g 1 2 E > '
const getMoves = 'Please type move sequence e.g MLRM > '

// process all input
// capture commands
lineReader.question(getBoundary, (boundary) => {
  // process boundary
  boundary = boundary.split(' ')
  if (isNaN(boundary[0]) || isNaN(boundary[1])) {
    console.log('Invalid boundary input')
    lineReader.close()
    return
  }
  boundary = { x: parseInt(boundary[0]), y: parseInt(boundary[1]) }

  // process location
  lineReader.question(getLocationDirection, (locationDirection) => {
    locationDirection = locationDirection.split(' ')

    if (isNaN(locationDirection[0]) || isNaN(locationDirection[1]) || 'NEWS'.indexOf(locationDirection[2]) < 0) {
      console.log('Invalid location and direction format')
      lineReader.close()
      return
    }
    locationDirection = { x: parseInt(locationDirection[0]), y: parseInt(locationDirection[1]), direction: locationDirection[2] }
    if (locationDirection.x < 0 || locationDirection.x > boundary.x || locationDirection.y < 0 || locationDirection.y > boundary.y) {
      console.log('Invalid current location')
      lineReader.close()
      return
    }

    // process moves
    lineReader.question(getMoves, (moves) => {
      if (moves.length === 0) {
        console.log('Invalid moves')
        lineReader.close()
        return
      }
      lineReader.close()

      // all validations have passed, proceed with movements
      // send list of commands
      const rover = new Rover(boundary, locationDirection, moves)
      rover.move()
      const destination = rover.getLocation()
      console.log(destination)
    })
  })
})
