const Rover = require('../rover')

test('Custom valid test case', () => {
  const rover = new Rover({ x: 8, y: 8 }, { x: 1, y: 2, direction: 'E' }, 'MMLMRMMRRMML')
  rover.move()
  const destination = rover.getLocation()
  expect(destination).toBe('3 3 S')
})

test('Given valid test case', () => {
  const rover = new Rover({ x: 5, y: 5 }, { x: 1, y: 2, direction: 'N' }, 'LMLMLMLMM')
  rover.move()
  const destination = rover.getLocation()
  expect(destination).toBe('1 3 N')
})

test('Custom invalid y move test case', () => {
  const rover = new Rover({ x: 8, y: 1 }, { x: 1, y: 2, direction: 'E' }, 'MMLMRMMRRMML')
  try {
    rover.move()
  } catch (error) {
    expect(error.message).toBe('Invalid y move')
  }
})
test('Custom invalid x move test case', () => {
  const rover = new Rover({ x: 2, y: 2 }, { x: 1, y: 2, direction: 'E' }, 'MMMMMMML')
  try {
    rover.move()
  } catch (error) {
    expect(error.message).toBe('Invalid x move')
  }
})
test('Custom invalid move test case', () => {
  const rover = new Rover({ x: 5, y: 5 }, { x: 1, y: 2, direction: 'E' }, 'MGMMMMMML')
  try {
    rover.move()
  } catch (error) {
    expect(error.message).toBe('Invalid move')
  }
})
